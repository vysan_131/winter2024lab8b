import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		Tile blankValue =Tile.BLANK; 
		System.out.println(blankValue.getName());
		Board myBoard = new Board();
		System.out.println(myBoard);
		Scanner scanner = new Scanner(System.in);
		int numCastles =7;
		int turns =0;
		System.out.println("Welcome to the Board game!");
		
		while(numCastles >0 && turns < 8){
			
			System.out.println(myBoard);
			System.out.println("Number of Castle: " +numCastles);
			System.out.println("Number of turns: "+turns);
			System.out.println("Enter a number as a row (0-4): ");
			int row = scanner.nextInt();
			System.out.println("Enter a number as a column (0-4): ");
			int col = scanner.nextInt();
			myBoard.placeToken(row, col);
			
			while(myBoard.placeToken(row, col) < 0){
				System.out.println("Invalid input! Please try again. ");
				System.out.println("Enter a number as a row (0-4): ");
				row = scanner.nextInt();
				System.out.println("Enter a number as a column (0-4): ");
				col = scanner.nextInt();
			}
			if(myBoard.placeToken(row, col) == 1){
				System.out.println("There's a wall at this position");
			}
			else{
				numCastles--;
				System.out.println("A castle tile has successfully placed");
			}
			turns++;
		}	
		System.out.println(myBoard);
		if(numCastles == 0){
			System.out.println("You won !!!");
		}
		else{
			System.out.println("You lost...Play again!!");
		}	
		
	}
}	
public enum Tile{
	BLANK("_"),
	WALL("_"),
	HIDDEN_WALL("W"),
	CASTLE("C");
	
	private final String name;
	
	private Tile(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}	
}	
import java.util.Random;
public class Board{
	private Tile[][] grid;
	private final int SIZE =5;
	
	public Board(){
		Random rand = new Random();
		Tile t = Tile.BLANK;
		
		this.grid = new Tile[SIZE][SIZE];
		
		for(int i=0;i<this.grid.length;i++){
			
			int randIndex = rand.nextInt(this.grid[i].length);
			
			for(int j=0;j<this.grid[i].length;j++){
				if(j == randIndex){
					this.grid[i][j] =Tile.HIDDEN_WALL;
				}
				else{
					this.grid[i][j] =t;
				}
			}
		}
		
	}	
	public String toString(){
		String result= "";
		for(int i=0; i<grid.length;i++){
			for(int j=0; j<grid[i].length;j++){
				result = result + grid[i][j].getName() + " ";
			}
		result = result + "\n";
		}
		
		return result;
	}	
	public int placeToken(int row, int col){
		Tile t = Tile.BLANK;
		
		if(row >= SIZE || col >= SIZE || row < 0 || col < 0){
			return -2;
		}
		else if(this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL){
			return -1;
		}
		else if(this.grid[row][col] == Tile.HIDDEN_WALL){
			this.grid[row][col] = Tile.CASTLE;
			return 1;
		}
		else{
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
			
	}
}